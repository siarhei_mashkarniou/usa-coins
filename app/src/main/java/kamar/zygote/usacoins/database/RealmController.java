package kamar.zygote.usacoins.database;

import io.realm.Realm;
import io.realm.RealmResults;
import kamar.zygote.usacoins.model.Backup;
import kamar.zygote.usacoins.model.CoinModel;
import kamar.zygote.usacoins.model.CollectionModel;

public class RealmController{

    static Realm realm;

    public static Realm getRealm(){
        return realm;
    }

    public static String getCollectionName(int id){
        realm = Realm.getDefaultInstance();
        return realm.where(CollectionModel.class).equalTo("id", id).findFirst().getName();
    }

    public static void setToCollections(CollectionModel collectionModel){
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.insertOrUpdate(collectionModel);
        realm.commitTransaction();
    }

    public static void setToCoins(CoinModel coinModel){
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.insertOrUpdate(coinModel);
        realm.commitTransaction();
    }

    public static RealmResults<CollectionModel>  getAllCollections(){
        realm = Realm.getDefaultInstance();
        return realm.where(CollectionModel.class).findAll();
    }

    public static CoinModel getCoinById(int id){
        realm = Realm.getDefaultInstance();
        return realm.where(CoinModel.class).equalTo("id", id).findFirst();
    }

    public static Backup getMyCoinById(int id){
        realm = Realm.getDefaultInstance();
        return realm.where(Backup.class).equalTo("coinId", id).findFirst();
    }

    public static void setMyCoin(Backup myCoin){
        realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.insertOrUpdate(myCoin);
        realm.commitTransaction();
    }

    public static RealmResults<CoinModel> getCoinsById(int id){
        realm = Realm.getDefaultInstance();
        return realm.where(CoinModel.class).equalTo("id_collection", id).findAll();
    }

    public static RealmResults<Backup> getMyCoinsById(int id){
        realm = Realm.getDefaultInstance();
        return realm.where(Backup.class).equalTo("collectionId", id).findAll();
    }

    public static RealmResults<Backup> getAllMyCoins(){
        realm = Realm.getDefaultInstance();
        return realm.where(Backup.class).findAll();
    }

    public static void cleanBackup(){
        realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.delete(Backup.class);
            }
        });
    }

    public static void closeRealm(){
        realm.close();
    }
}
