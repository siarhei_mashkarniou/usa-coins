package kamar.zygote.usacoins.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import kamar.zygote.usacoins.model.CoinModel;
import kamar.zygote.usacoins.model.CollectionModel;

public class DatabaseAccess {

    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;

    private String CollectionTable = "new_collections";
    private String CoinsTable = "new_coins";
    private String COLLECTION_ID = "_id";
    private String COLLECTION_NAME = "name";
    private String COLLECTION_IMAGE = "image";
    private String COLLECTION_YEARS = "years";

    private String COINS_ID = "_id";
    private String COINS_ID_COLLECTION = "id_collection";
    private String COINS_NAME = "name";
    private String COINS_MINTAGE = "mintage";
    private String COINS_DENOMINATION = "denomination";
    private String COINS_WEIGHT = "weight";
    private String COINS_DIAMETER = "diameter";
    private String COINS_THICKNESS = "thickness";
    private String COINS_METALL = "metall";
    private String COINS_YEAR = "year";
    private String COINS_PRICE = "price";
    private String COINS_MINT = "mint";
    private String COINS_IMAGE_1 = "image_1";
    private String COINS_IMAGE_2 = "image_2";

    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    public void getCollections() {
        Cursor cursor = database.rawQuery("SELECT * FROM " + CollectionTable, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            if(cursor.getInt(cursor.getColumnIndex(COLLECTION_ID)) != 0){
                CollectionModel collectionModel = new CollectionModel();
                collectionModel.setId(cursor.getInt(cursor.getColumnIndex(COLLECTION_ID)));
                collectionModel.setImage("http://kam32ar.com/image?id=" + cursor.getString(cursor.getColumnIndex(COLLECTION_IMAGE)));
                collectionModel.setName(cursor.getString(cursor.getColumnIndex(COLLECTION_NAME)));
                collectionModel.setYears(cursor.getString(cursor.getColumnIndex(COLLECTION_YEARS)));
                RealmController.setToCollections(collectionModel);
                getCoins(collectionModel.getId());
            }
            cursor.moveToNext();
        }
        cursor.close();
    }

    public ArrayList<CoinModel> getCoins(float collection_id) {
        ArrayList<CoinModel> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM " + CoinsTable + " where id_collection = " + collection_id, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            CoinModel coin = new CoinModel();
            coin.setId(cursor.getInt(cursor.getColumnIndex(COINS_ID)));
            coin.setName(cursor.getString(cursor.getColumnIndex(COINS_NAME)));
            coin.setMintage(cursor.getString(cursor.getColumnIndex(COINS_MINTAGE)));
            coin.setId_collection(cursor.getInt(cursor.getColumnIndex(COINS_ID_COLLECTION)));
            coin.setDenomination(cursor.getString(cursor.getColumnIndex(COINS_DENOMINATION)));
            coin.setWeight(cursor.getFloat(cursor.getColumnIndex(COINS_WEIGHT)));
            coin.setDiameter(cursor.getFloat(cursor.getColumnIndex(COINS_DIAMETER)));
            coin.setThickness(cursor.getFloat(cursor.getColumnIndex(COINS_THICKNESS)));
            coin.setMetall(cursor.getString(cursor.getColumnIndex(COINS_METALL)));
            coin.setYear(cursor.getString(cursor.getColumnIndex(COINS_YEAR)));
            coin.setPrice(cursor.getFloat(cursor.getColumnIndex(COINS_PRICE)));
            coin.setMint(cursor.getString(cursor.getColumnIndex(COINS_MINT)));
            coin.setImage_1("http://kam32ar.com/image?id=" + cursor.getString(cursor.getColumnIndex(COINS_IMAGE_1)));
            coin.setImage_2("http://kam32ar.com/image?id=" + cursor.getString(cursor.getColumnIndex(COINS_IMAGE_2)));
            RealmController.setToCoins(coin);
            list.add(coin);
            cursor.moveToNext();
        }

        cursor.close();
        return list;
    }

}
