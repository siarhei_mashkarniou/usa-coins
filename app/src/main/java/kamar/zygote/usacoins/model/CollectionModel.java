package kamar.zygote.usacoins.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class CollectionModel extends RealmObject{

    @PrimaryKey
    private int id;
    private String name;
    private String image;
    private String years;
    private RealmList<CoinModel> coinModel;

    public CollectionModel(){
        this.id = 0;
        this.name = "";
        this.image = "";
        this.years = "";
        this.coinModel = new RealmList<>();
    }

    public RealmList<CoinModel> getCoinModel() {
        return coinModel;
    }

    public void setCoinModel(RealmList<CoinModel> coinModel) {
        this.coinModel = coinModel;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getYears() {
        return years;
    }

    public void setYears(String years) {
        this.years = years;
    }
}
