package kamar.zygote.usacoins.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class CoinModel extends RealmObject {

    @PrimaryKey
    private int id;
    private int id_collection;
    private String name;
    private String denomination;
    private float weight;
    private float diameter;
    private float thickness;
    private String metall;
    private String year;
    private String mintage;
    private float price;
    private String mint;
    private String image_1;
    private String image_2;
    private Backup myCoin;

    public CoinModel(){
        this.id = 0;
        this.name = "";
        this.id_collection = 0;
        this.name = "";
        this.denomination = "";
        this.weight = 0;
        this.diameter = 0;
        this.thickness = 0;
        this.metall = "";
        this.year = "";
        this.mintage = "";
        this.price = 0;
        this.mint = "";
        this.image_1 = "";
        this.image_2 = "";
        this.myCoin = null;
    }

    public Backup getMyCoin() {
        return myCoin;
    }

    public void setMyCoin(Backup myCoin) {
        this.myCoin = myCoin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_collection() {
        return id_collection;
    }

    public void setId_collection(int id_collection) {
        this.id_collection = id_collection;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDenomination() {
        return denomination;
    }

    public void setDenomination(String denomination) {
        this.denomination = denomination;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getDiameter() {
        return diameter;
    }

    public void setDiameter(float diameter) {
        this.diameter = diameter;
    }

    public float getThickness() {
        return thickness;
    }

    public void setThickness(float thickness) {
        this.thickness = thickness;
    }

    public String getMetall() {
        return metall;
    }

    public void setMetall(String metall) {
        if(metall != null){
            this.metall = metall;
        }else{
            this.metall = "";
        }
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        if(year != null){
            this.year = year;
        }else{
            this.year = "";
        }
    }

    public String getMintage() {
        return mintage;
    }

    public void setMintage(String mintage) {
        this.mintage = mintage;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getMint() {
        return mint;
    }

    public void setMint(String mint) {
        this.mint = mint;
    }

    public String getImage_1() {
        return image_1;
    }

    public void setImage_1(String image_1) {
        this.image_1 = image_1;
    }

    public String getImage_2() {
        return image_2;
    }

    public void setImage_2(String image_2) {
        this.image_2 = image_2;
    }
}


