package kamar.zygote.usacoins.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Registration {

    @SerializedName("success")
    @Expose
    private Integer success;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getSuccess() {
        return success;
    }

    public void setSuccess(Integer success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

}

class Data {

    @SerializedName("balance")
    @Expose
    private Integer balance;
    @SerializedName("tickets")
    @Expose
    private Integer tickets;
    @SerializedName("prequest")
    @Expose
    private Integer prequest;
    @SerializedName("amountMin")
    @Expose
    private Integer amountMin;
    @SerializedName("hoardLeftTime")
    @Expose
    private Integer hoardLeftTime;
    @SerializedName("maxDayOut")
    @Expose
    private Integer maxDayOut;

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getTickets() {
        return tickets;
    }

    public void setTickets(Integer tickets) {
        this.tickets = tickets;
    }

    public Integer getPrequest() {
        return prequest;
    }

    public void setPrequest(Integer prequest) {
        this.prequest = prequest;
    }

    public Integer getAmountMin() {
        return amountMin;
    }

    public void setAmountMin(Integer amountMin) {
        this.amountMin = amountMin;
    }

    public Integer getHoardLeftTime() {
        return hoardLeftTime;
    }

    public void setHoardLeftTime(Integer hoardLeftTime) {
        this.hoardLeftTime = hoardLeftTime;
    }

    public Integer getMaxDayOut() {
        return maxDayOut;
    }

    public void setMaxDayOut(Integer maxDayOut) {
        this.maxDayOut = maxDayOut;
    }

}