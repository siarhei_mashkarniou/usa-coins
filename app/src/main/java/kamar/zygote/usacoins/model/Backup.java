package kamar.zygote.usacoins.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Backup implements RealmModel {

    @SerializedName("coin_id")
    @Expose
    @PrimaryKey
    private Integer coinId;
    @SerializedName("collection_id")
    @Expose
    private Integer collectionId;
    @SerializedName("count")
    @Expose
    private RealmList<Count> count = new RealmList<>();
    @SerializedName("note")
    @Expose
    private String note;

    public Integer getCoinId() {
        return coinId;
    }

    public void setCoinId(Integer coinId) {
        this.coinId = coinId;
    }

    public Integer getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(Integer collectionId) {
        this.collectionId = collectionId;
    }

    public RealmList<Count> getCount() {
        return count;
    }

    public void setCount(RealmList<Count> count) {
        this.count = count;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}


