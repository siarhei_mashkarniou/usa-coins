package kamar.zygote.usacoins.fragment;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.RealmResults;
import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.activity.GeneralActivity;
import kamar.zygote.usacoins.adapter.CollectionCatalogAdapter;
import kamar.zygote.usacoins.core.FragmentIvents;
import kamar.zygote.usacoins.core.FragmentsArray;
import kamar.zygote.usacoins.core.ToolbarController;
import kamar.zygote.usacoins.database.RealmController;
import kamar.zygote.usacoins.model.CoinModel;
import kamar.zygote.usacoins.mvp.presenter.CollectionCatalogPresenter;
import kamar.zygote.usacoins.mvp.view.CollectionCatalogView;

public class CollectionCatalogFragment extends MvpAppCompatFragment implements CollectionCatalogView {

    @BindView(R.id.list)
    RecyclerView list;

    private Unbinder unbinder;
    private FragmentIvents fragmentIvents;
    private Bundle args;
    private int ID;
    private GridLayoutManager layoutManager;
    @InjectPresenter
    public CollectionCatalogPresenter presenter;
    public Parcelable state;

    public CollectionCatalogFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        args = getArguments();
        ID = args.getInt("id");
    }

    @Override
    public void onResume(){
        super.onResume();
        fragmentIvents = (GeneralActivity)getActivity();
        fragmentIvents.setCurrentFragment(this);
        ToolbarController.setToolbarTitle(RealmController.getCollectionName(ID));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_collection_catalog, container, false);
        unbinder = ButterKnife.bind(this, root);

        presenter.getData(ID);

        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        fragmentIvents = null;
    }

    @Override
    public void setDataToList(RealmResults<CoinModel> coins) {
        list.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getActivity(), 3);
        list.setLayoutManager(layoutManager);

        list.setAdapter(new CollectionCatalogAdapter(coins, new CollectionCatalogAdapter.OnItemClickListener() {
            @Override public void onItemClick(CoinModel item) {
                state = list.getLayoutManager().onSaveInstanceState();
                fragmentIvents.onClickFragment(FragmentsArray.COIN_FRAGMENT, item.getId());
            }
        }));
        list.getLayoutManager().onRestoreInstanceState(state);
    }
}
