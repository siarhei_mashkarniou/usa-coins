package kamar.zygote.usacoins.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import org.aviran.cookiebar2.CookieBar;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import butterknife.Unbinder;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.activity.StartActivity;
import kamar.zygote.usacoins.adapter.AccountAdapter;
import kamar.zygote.usacoins.core.CoinApp;
import kamar.zygote.usacoins.core.DialogsControl;
import kamar.zygote.usacoins.core.Saver;
import kamar.zygote.usacoins.core.ToolbarController;
import kamar.zygote.usacoins.database.RealmController;

public class AccountFragment extends Fragment implements DialogsControl.PhotoChoser{

    @BindView(R.id.photo) ImageView photo;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.list) ListView list;
    @BindView(R.id.imageUpPhoto) ImageView upPhoto;
    @BindView(R.id.imageName) ImageView imageName;
    @BindView(R.id.textVersion)
    TextView textVersion;
    private ArrayList<String> data;
    private AccountAdapter adapter;
    private Unbinder unbinder;
    private Saver saver;

    private int GALLERY = 1;
    private int CAMERA = 2;
    private FirebaseUser user;

    public AccountFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ToolbarController.setToolbarTitle("Account");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_account, container, false);
        unbinder = ButterKnife.bind(this, root);

        saver = new Saver(getActivity());

        getData();

        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            String nameString = user.getDisplayName();
            String email = user.getEmail();
            Uri photoUrl = user.getPhotoUrl();

            if(email != null && (nameString == null || nameString.length() < 1)){
                name.setText(email);
            }else{
                name.setText(nameString);
            }

            Glide.with(CoinApp.getContext())
                    .load(photoUrl)
                    .bitmapTransform(new CropCircleTransformation(getActivity()))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.coin_ic)
                    .error(R.drawable.coin_ic)
                    .into(photo);

            String uid = user.getUid();
        }

        name.setEnabled(false);

        textVersion.setText("Version: " + CoinApp.getVersion());

        return root;
    }

    @SuppressLint("WrongConstant")
    @OnClick(R.id.imageUpPhoto)
    void onUpPhoto(){
        int hasWriteContactsPermission = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            hasWriteContactsPermission = getActivity().checkSelfPermission(Manifest.permission.CAMERA);
            if (hasWriteContactsPermission != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[] {Manifest.permission.CAMERA}, 123);
                return;
            }else{
                DialogsControl.showPicDialog(getActivity(), this);
            }
        } else{
            DialogsControl.showPicDialog(getActivity(), this);
        }
    }

    @OnClick(R.id.imageName)
    void onImageName(){
        name.setEnabled(true);
        name.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(name, InputMethodManager.SHOW_IMPLICIT);
        name.setSelection(name.getText().length());
        name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    name.setEnabled(false);
                    user = FirebaseAuth.getInstance().getCurrentUser();
                    if (user != null) {
                        saveToFireName(name.getText().toString());
                    }
                    InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 123:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    DialogsControl.showPicDialog(getActivity(), this);
                } else {
                    showCookie("CAMERA Denied", "Ups!");
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @OnItemClick(R.id.list)
    void listClick(int position){
        if (position == 0) {
            exit();
        }
    }

    private void getData() {
        data = new ArrayList<>();
        data.add(this.getResources().getString(R.string.out));
        setDataToList();
    }

    private void setDataToList() {
        adapter = new AccountAdapter(getActivity(), data);
        list.setAdapter(adapter);
    }

    private void exit(){
        FirebaseAuth.getInstance().signOut();
        saver.setMail("");
        saver.setPassword("");
        RealmController.cleanBackup();
        Intent intent = new Intent(getActivity(), StartActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void galery() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(galleryIntent, GALLERY);
    }

    @Override
    public void camera() {
        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), contentURI);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                    Glide.with(CoinApp.getContext())
                            .load(stream.toByteArray())
                            .bitmapTransform(new CropCircleTransformation(getActivity()))
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.coin_ic)
                            .into(photo);
                    saveToFire(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    showCookie("Failed!", "Ups!");
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            thumbnail.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Glide.with(CoinApp.getContext())
                    .load(stream.toByteArray())
                    .bitmapTransform(new CropCircleTransformation(getActivity()))
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.coin_ic)
                    .into(photo);
            saveToFire(thumbnail);
        }
    }

    private void showCookie(String message, String title){
        if(this.isAdded()){
            DialogsControl.closeProgressDialog();
            CookieBar.Build(getActivity())
                    .setTitle(title)
                    .setMessage(message)
                    .setDuration(3000)
                    .show();
        }
    }

    private void saveToFire(Bitmap bitmap){
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setPhotoUri(getImageUri(getActivity(), bitmap))
                .build();
        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            AuthCredential credential = EmailAuthProvider.getCredential(saver.getMail(), saver.getPassword());
                            user.reauthenticate(credential)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                showCookie("User profile updated.", "OK");
                                            }else{
                                                showCookie(task.getException().getMessage(), "Ups!");
                                            }
                                        }
                                    });
                        }else{
                            showCookie(task.getException().getMessage(), "Ups!");
                        }
                    }
                });
    }

    private void saveToFireName(String name){
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(name)
                .build();
        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            AuthCredential credential = EmailAuthProvider.getCredential(saver.getMail(), saver.getPassword());
                            user.reauthenticate(credential)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                showCookie("User profile updated.", "Ok");
                                            }else{
                                                showCookie(task.getException().getMessage(), "Ups!");
                                            }
                                        }
                                    });
                        }else{
                            showCookie(task.getException().getMessage(), "Ups!");
                        }
                    }
                });
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
}
