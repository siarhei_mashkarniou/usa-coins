package kamar.zygote.usacoins.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnTextChanged;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;
import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.activity.GeneralActivity;
import kamar.zygote.usacoins.adapter.SearchAdapter;
import kamar.zygote.usacoins.core.FragmentIvents;
import kamar.zygote.usacoins.core.FragmentsArray;
import kamar.zygote.usacoins.core.ToolbarController;
import kamar.zygote.usacoins.model.Backup;
import kamar.zygote.usacoins.model.CoinModel;

import static kamar.zygote.usacoins.core.SearchMagic.getSearch;

public class SearchFragment extends Fragment {

    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.textEmpty)
    TextView emptyText;
    @BindView(R.id.searchText)
    EditText searchText;

    private RealmResults<CoinModel> allCoins;
    private RealmList<CoinModel> searchCoin;
    private RequestSearch RS;
    private String search;
    private Unbinder unbinder;
    private Realm realm;
    private FragmentIvents fragmentIvents;
    private LinearLayoutManager layoutManager;

    public SearchFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentIvents = (GeneralActivity)getActivity();
    }

    @Override
    public void onResume(){
        super.onResume();
        fragmentIvents.setCurrentFragment(this);
        ToolbarController.setToolbarTitle("Search");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search, container, false);
        unbinder = ButterKnife.bind(this, root);
        realm = Realm.getDefaultInstance();

        list.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        list.setLayoutManager(layoutManager);

        setDataToList();

        RS = new RequestSearch();

        return root;
    }

    @OnTextChanged(R.id.searchText)
    public void onChange(){
        searchText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(searchText.getWindowToken(), 0);
                    return true;
                }
                return false;
            }
        });

        cancelTask();

        RS = new RequestSearch();
        search = searchText.getText().toString().toLowerCase();
        searchCoin = new RealmList<>();
        searchCoin.clear();
        list.setEnabled(false);
        RS.doInBackground();
    }

    private void cancelTask() {
        if (RS == null)
            return;
    }

    private void setDataToList() {
        allCoins = realm.where(CoinModel.class).findAll();
        RealmList <CoinModel> results = new RealmList<CoinModel>();
        RealmResults<Backup> myCoins = realm.where(Backup.class).findAll();
        results.addAll(allCoins.subList(0, allCoins.size()));
        for(int i = 0; i < results.size(); i++){
            for(int j = 0; j < myCoins.size(); j++) {
                if (results.get(i).getId() == myCoins.get(j).getCoinId()) {
                    realm.beginTransaction();
                    results.get(i).setMyCoin(myCoins.get(j));
                    realm.commitTransaction();
                }
            }
        }

        RealmList <CoinModel> coinTemp = new RealmList<CoinModel>();
        coinTemp.addAll(allCoins.subList(0, allCoins.size()));

        list.setAdapter(new SearchAdapter(coinTemp, new SearchAdapter.OnItemClickListener() {
            @Override public void onItemClick(CoinModel item) {
                fragmentIvents.onClickFragment(FragmentsArray.COIN_FRAGMENT, item.getId());
            }
        }));
    }

    class RequestSearch  {

        protected void doInBackground() {

            String name;
            String metall;
            String date;
            boolean offset;
            boolean offsetM;
            boolean offsetD;

            searchCoin.removeAll(searchCoin);

            for (int i = 0; i < allCoins.size(); i++) {
                name = allCoins.get(i).getName();
                metall = allCoins.get(i).getMetall();
                date = allCoins.get(i).getYear();
                offset = name.toLowerCase().contains(search);
                offsetM = metall.toLowerCase().contains(search);
                offsetD = date.toLowerCase().contains(search);

                if (offset == true || offsetM == true || offsetD == true) {
                    CoinModel coin = new CoinModel();
                    List<String> resultSearch = getSearch(search, name, metall, date);

                    if(offset == true){
                        coin.setName(resultSearch.get(0));
                    }else{
                        coin.setName(allCoins.get(i).getName());
                    }

                    if(offsetM == true){
                        coin.setMetall(resultSearch.get(1));
                    }else{
                        coin.setMetall(allCoins.get(i).getMetall());
                    }

                    if(offsetD == true){
                        coin.setYear(resultSearch.get(2));
                    }else{
                        coin.setYear(allCoins.get(i).getYear());
                    }

                    coin.setImage_2(allCoins.get(i).getImage_2());
                    coin.setImage_1(allCoins.get(i).getImage_1());
                    coin.setPrice(allCoins.get(i).getPrice());
                    coin.setDenomination(allCoins.get(i).getDenomination());
                    coin.setDiameter(allCoins.get(i).getDiameter());
                    coin.setId(allCoins.get(i).getId());
                    coin.setId_collection(allCoins.get(i).getId_collection());
                    coin.setMint(allCoins.get(i).getMint());
                    coin.setMintage(allCoins.get(i).getMintage());
                    coin.setThickness(allCoins.get(i).getThickness());
                    coin.setWeight(allCoins.get(i).getWeight());
                    realm.beginTransaction();
                    Backup myCoin = realm.where(Backup.class).equalTo
                            ("coinId", coin.getId()).findFirst();
                    if(myCoin != null){
                        coin.setMyCoin(myCoin);
                    }
                    realm.commitTransaction();
                    searchCoin.add(coin);
                }
            }
            onPostExecute();
        }

        public void onPostExecute() {
            Collections.sort(searchCoin, new Comparator<CoinModel>() {
                public int compare(CoinModel s1, CoinModel s2) {
                    return Integer.valueOf(s1.getId()) - Integer.valueOf(s2.getId());
                }
            });

            list.setAdapter(new SearchAdapter(searchCoin, new SearchAdapter.OnItemClickListener() {
                @Override public void onItemClick(CoinModel item) {
                    fragmentIvents.onClickFragment(FragmentsArray.COIN_FRAGMENT, item.getId());
                }
            }));

            if (searchCoin.size() < 1) {
                emptyText.setVisibility(View.VISIBLE);
            } else {
                emptyText.setVisibility(View.GONE);
            }
            list.setEnabled(true);
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unbinder.unbind();
        fragmentIvents = null;
    }
}
