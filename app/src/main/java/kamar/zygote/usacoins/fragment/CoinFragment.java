package kamar.zygote.usacoins.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.yarolegovich.discretescrollview.DiscreteScrollView;
import com.yarolegovich.discretescrollview.transform.ScaleTransformer;

import org.aviran.cookiebar2.CookieBar;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.RealmResults;
import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.activity.GeneralActivity;
import kamar.zygote.usacoins.adapter.CoinScrollAdapter;
import kamar.zygote.usacoins.core.CoinApp;
import kamar.zygote.usacoins.core.DialogsControl;
import kamar.zygote.usacoins.core.Saver;
import kamar.zygote.usacoins.core.ToolbarController;
import kamar.zygote.usacoins.database.RealmController;
import kamar.zygote.usacoins.model.Backup;
import kamar.zygote.usacoins.model.BackupFromServer;
import kamar.zygote.usacoins.model.CoinModel;
import kamar.zygote.usacoins.model.Count;
import kamar.zygote.usacoins.ui.CoinsTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class CoinFragment extends Fragment {

    @BindView(R.id.picker)
    DiscreteScrollView scrollView;
    @BindView(R.id.textDate)
    CoinsTextView textDate;
    @BindView(R.id.textDenom)
    CoinsTextView textDenom;
    @BindView(R.id.textMint)
    CoinsTextView textMint;
    @BindView(R.id.textPrice)
    CoinsTextView textPrice;
    @BindView(R.id.textMetall)
    CoinsTextView textMetall;
    @BindView(R.id.textWeight)
    CoinsTextView textWeight;
    @BindView(R.id.textDiam)
    CoinsTextView textDiam;
    @BindView(R.id.textThick)
    CoinsTextView textThick;
    @BindView(R.id.rootParams)
    RelativeLayout rootParams;
    @BindView(R.id.buy)
    CardView buyButton;
    @BindView(R.id.sell)
    CardView sellButton;
    private Unbinder unbinder;
    private GeneralActivity activity;
    private Backup myCoin;
    private int lastView = 0;
    private CoinScrollAdapter scrollAdapter;
    private CoinModel coinModel;
    private String[] mintsArray;
    private Bundle args;
    private int ID;
    private Saver saver;

    public CoinFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = (GeneralActivity) getActivity();
        args = getArguments();
        ID = args.getInt("id");
        coinModel = RealmController.getCoinById(ID);
        myCoin = RealmController.getMyCoinById(ID);
        ToolbarController.setToolbarTitle(coinModel.getName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.fragment_coin, container, false);
        unbinder = ButterKnife.bind(this, root);

        activity = (GeneralActivity) getActivity();
        saver = new Saver(activity);

        if(myCoin == null){
            myCoin = new Backup();
            myCoin.setCollectionId(RealmController.getCoinById(ID).getId_collection());
            myCoin.setCoinId(ID);
            RealmController.setMyCoin(myCoin);
        }

        setImage();
        setCoinDataToArray();
        getMints();
        setNoteView();

        return root;
    }

    @OnClick(R.id.buy)
    void clickBuy(){
        showCookie("In the following updates");
    }

    @OnClick(R.id.sell)
    void clickSell(){
        showCookie("In the following updates");
    }

    public void showCookie(String message){
        DialogsControl.closeProgressDialog();
        CookieBar.Build(getActivity())
                .setTitle("Ups!")
                .setMessage(message)
                .setDuration(3000)
                .show();
    }

    private void getMints() {
        String mints = coinModel.getMint();
        mintsArray = mints.split(";");
        for(int i = 0; i < mintsArray.length; i++){
            if(myCoin.getCount() != null && myCoin.getCount().size() > i){

            }else{
                RealmController.getRealm().beginTransaction();
                myCoin.getCount().add(i, new Count(mintsArray[i]));
                RealmController.getRealm().commitTransaction();
            }
        }
        setMints();
    }

    private void setMints() {
        for (int i = 0; i < mintsArray.length; i++) {
            final int[] countMint = {0};
            countMint[0] = myCoin.getCount().get(i).getCount();

            final int mintNumber = i;
            LayoutInflater inflater2 = (LayoutInflater) activity.getSystemService(LAYOUT_INFLATER_SERVICE);
            final View childLayout = inflater2.inflate(R.layout.item_coin_count, (ViewGroup) activity.findViewById(R.id.child));
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            if (i == 0) {
                params.addRule(RelativeLayout.BELOW, R.id.param_2);
                lastView = R.id.child;
            } else {
                params.addRule(RelativeLayout.BELOW, lastView);
                lastView = lastView + i;
            }
            params.setMargins(dpToPx(10), dpToPx(10), dpToPx(10), 0);
            rootParams.addView(childLayout, params);

            ImageView minus = (ImageView) childLayout.findViewById(R.id.minus);
            ImageView plus = (ImageView) childLayout.findViewById(R.id.plus);
            TextView mint = (TextView) childLayout.findViewById(R.id.mint);
            final TextView count = (TextView) childLayout.findViewById(R.id.count);

            mint.setText(myCoin.getCount().get(i).getName());
            count.setText(countMint[0] + "");
            minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (countMint[0] > 0) {
                        countMint[0] = countMint[0] - 1;
                    }
                    count.setText(countMint[0] + "");
                    saveMyCoinCount(mintNumber, countMint[0]);
                }
            });

            plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    countMint[0] = countMint[0] + 1;
                    count.setText(countMint[0] + "");
                    saveMyCoinCount(mintNumber, countMint[0]);
                }
            });
            childLayout.setId(lastView);
        }
    }

    private void setNoteView() {
        LayoutInflater inflater2 = (LayoutInflater) activity.getSystemService(LAYOUT_INFLATER_SERVICE);
        final View childLayout = inflater2.inflate(R.layout.item_coin_note, (ViewGroup) activity.findViewById(R.id.childNote));
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (lastView == 0) {
            params.addRule(RelativeLayout.BELOW, R.id.param_2);
        } else {
            params.addRule(RelativeLayout.BELOW, lastView);
        }
        params.setMargins(dpToPx(10), dpToPx(10), dpToPx(10), 0);
        rootParams.addView(childLayout, params);
        final EditText edit = (EditText) childLayout.findViewById(R.id.note);
        edit.setText(myCoin.getNote());
        edit.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                saveMyCoinNote(edit.getText().toString());
            }
        });
        childLayout.setId(lastView + 1);
    }

    private void setImage() {
        ArrayList<String> images = new ArrayList<>();
        images.add(coinModel.getImage_1());
        images.add(coinModel.getImage_2());
        scrollAdapter = new CoinScrollAdapter(images);
        scrollView.setAdapter(scrollAdapter);
        scrollView.setItemTransformer(new ScaleTransformer.Builder().setMinScale(0.7f).build());
    }

    private void setCoinDataToArray() {
        textDate.setText(coinModel.getYear());
        textDenom.setText(coinModel.getDenomination() + " $");
        textMint.setText(coinModel.getMintage() + "");
        //textPrice.setText(coinModel.getPrice() + "");
        textMetall.setText(coinModel.getMetall());
        textWeight.setText(coinModel.getWeight() + " g");
        textDiam.setText(coinModel.getDiameter() + " mm");
        textThick.setText(coinModel.getThickness() + " mm");
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private void saveMyCoinCount(int mint, int count) {
        RealmController.getRealm().beginTransaction();
        myCoin.getCount().get(mint).setCount(count);
        RealmController.getRealm().insertOrUpdate(myCoin);
        RealmController.getRealm().commitTransaction();
        sendBackup();
    }

    private void saveMyCoinNote(String note) {
        RealmController.getRealm().beginTransaction();
        myCoin.setNote(note);
        RealmController.getRealm().insertOrUpdate(myCoin);
        RealmController.getRealm().commitTransaction();
        sendBackup();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void sendBackup(){
        RealmResults<Backup> myCoins = RealmController.getAllMyCoins();
        String json = new Gson().toJson(RealmController.getRealm().copyFromRealm(myCoins));
        CoinApp.getHttpApi().sendBackup(CoinApp.getId(), json)
                .enqueue(new Callback<BackupFromServer>() {
                    @Override
                    public void onResponse(Call<BackupFromServer> call, Response<BackupFromServer> response) {
                        BackupFromServer auth = response.body();
                        if(auth != null){
                            int success = auth.getSuccess();
                            if(success == 2){

                            }else{

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BackupFromServer> call, Throwable t) {

                    }
                });
    }
}
