package kamar.zygote.usacoins.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.RealmResults;
import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.activity.GeneralActivity;
import kamar.zygote.usacoins.adapter.CatalogAdapter;
import kamar.zygote.usacoins.core.FragmentIvents;
import kamar.zygote.usacoins.core.FragmentsArray;
import kamar.zygote.usacoins.core.ToolbarController;
import kamar.zygote.usacoins.model.CollectionModel;
import kamar.zygote.usacoins.mvp.presenter.CtatalogPresenter;
import kamar.zygote.usacoins.mvp.view.CatalogView;

public class CatalogFragment extends MvpAppCompatFragment implements CatalogView{

    @BindView(R.id.list)
    RecyclerView list;
    @InjectPresenter
    CtatalogPresenter catalogPresenter;

    private Unbinder unbinder;
    private GridLayoutManager layoutManager;
    private FragmentIvents fragmentIvents;

    public CatalogFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onResume(){
        super.onResume();
        ToolbarController.setToolbarTitle("Catalog");
        fragmentIvents = (GeneralActivity)getActivity();
        fragmentIvents.setCurrentFragment(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_catalog, container, false);
        unbinder = ButterKnife.bind(this, root);

        catalogPresenter.getDataFromBase();

        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        fragmentIvents = null;
    }

    @Override
    public void setDataToList(RealmResults<CollectionModel> collections) {
        list.setHasFixedSize(true);
        layoutManager = new GridLayoutManager(getActivity(), 3);
        list.setLayoutManager(layoutManager);

        list.setAdapter(new CatalogAdapter(collections, new CatalogAdapter.OnItemClickListener() {
            @Override public void onItemClick(CollectionModel item) {
                fragmentIvents.onClickFragment(FragmentsArray.COLLECTION_CATALOG_FRAGMENT, item.getId());
            }
        }));
    }
}