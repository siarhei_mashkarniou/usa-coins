package kamar.zygote.usacoins.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmResults;
import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.activity.GeneralActivity;
import kamar.zygote.usacoins.adapter.MyCollectionAdapter;
import kamar.zygote.usacoins.core.DialogsControl;
import kamar.zygote.usacoins.core.FragmentIvents;
import kamar.zygote.usacoins.core.FragmentsArray;
import kamar.zygote.usacoins.core.ToolbarController;
import kamar.zygote.usacoins.model.Backup;
import kamar.zygote.usacoins.model.CoinModel;
import kamar.zygote.usacoins.model.CollectionModel;

public class MyCollectionFragment extends Fragment {

    @BindView(R.id.textPrice)
    TextView textPrice;
    @BindView(R.id.textTotalCoins)
    TextView textTotalCoins;
    @BindView(R.id.list)
    ListView list;
    private Unbinder unbinder;
    private GeneralActivity activity;
    private int allMyCoinsCounter = 0;
    private MyCollectionAdapter adapter;
    private Realm realm;
    private RealmResults<CollectionModel> collections;
    private RealmResults<CoinModel> coins;
    private RealmResults<Backup> myCoins;
    private FragmentIvents fragmentIvents;

    public MyCollectionFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentIvents = (GeneralActivity) getActivity();
    }

    @Override
    public void onResume() {
        super.onResume();
        ToolbarController.setToolbarTitle("My collection");
        fragmentIvents.setCurrentFragment(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_collection, container, false);

        activity = (GeneralActivity) getActivity();
        unbinder = ButterKnife.bind(this, root);
        realm = Realm.getDefaultInstance();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                fragmentIvents.onClickFragment(FragmentsArray.COLLECTION_CATALOG_FRAGMENT, collections.get(position).getId());
            }
        });

        getData();

        return root;
    }

    private void getData(){
        DialogsControl.startProgressDialog(getActivity());
        collections = realm.where(CollectionModel.class).findAll();
        coins = realm.where(CoinModel.class).findAll();
        myCoins = realm.where(Backup.class).findAll();
        getAllMyCoinsTotal();
        setDataToList();
    }

    private void setDataToList() {
        adapter = new MyCollectionAdapter(activity, collections, coins, myCoins);
        list.setAdapter(adapter);
    }

    private void getAllMyCoinsTotal() {
        allMyCoinsCounter = 0;
        for(int i = 0; i < myCoins.size(); i++){
            for(int j = 0; j < myCoins.get(i).getCount().size(); j++){
                allMyCoinsCounter = allMyCoinsCounter + myCoins.get(i).getCount().get(j).getCount();
            }
        }
        textTotalCoins.setText(allMyCoinsCounter + "");
        DialogsControl.closeProgressDialog();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        realm.close();
    }
}
