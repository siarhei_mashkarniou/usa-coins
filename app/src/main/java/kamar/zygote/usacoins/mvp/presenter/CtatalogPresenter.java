package kamar.zygote.usacoins.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import io.realm.RealmResults;
import kamar.zygote.usacoins.database.RealmController;
import kamar.zygote.usacoins.model.CollectionModel;
import kamar.zygote.usacoins.mvp.view.CatalogView;

@InjectViewState
public class CtatalogPresenter extends MvpPresenter<CatalogView>{

    private RealmResults<CollectionModel> collections;

    public void getDataFromBase(){
        collections = RealmController.getAllCollections();
        getViewState().setDataToList(collections);
    }
}
