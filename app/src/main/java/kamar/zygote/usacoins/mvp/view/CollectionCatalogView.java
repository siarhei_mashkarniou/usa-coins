package kamar.zygote.usacoins.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import io.realm.RealmResults;
import kamar.zygote.usacoins.model.CoinModel;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface CollectionCatalogView extends MvpView {

    void setDataToList(RealmResults<CoinModel> coins);
}
