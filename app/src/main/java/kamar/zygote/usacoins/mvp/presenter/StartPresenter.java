package kamar.zygote.usacoins.mvp.presenter;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;

import kamar.zygote.usacoins.core.CoinApp;
import kamar.zygote.usacoins.core.DialogsControl;
import kamar.zygote.usacoins.core.Saver;
import kamar.zygote.usacoins.database.RealmController;
import kamar.zygote.usacoins.model.Auth;
import kamar.zygote.usacoins.model.Backup;
import kamar.zygote.usacoins.model.BackupFromServer;
import kamar.zygote.usacoins.model.Registration;
import kamar.zygote.usacoins.mvp.view.StartView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@InjectViewState
public class StartPresenter extends MvpPresenter<StartView>{

    public void setAuth(){
        CoinApp.getHttpApi().auth(CoinApp.getId(), CoinApp.getContext().getPackageName(), CoinApp.getVersion())
                .enqueue(new Callback<Auth>() {
            @Override
            public void onResponse(Call<Auth> call, Response<Auth> response) {
                Auth auth = response.body();
                int success = -1;
                if(auth != null){
                    success = auth.getSuccess();
                }
                if(success == 0){

                }else{
                    getViewState().showCloseAppDialog();
                }
            }

            @Override
            public void onFailure(Call<Auth> call, Throwable t) {
                getViewState().showCookie("An error occurred during networking", "Ups!");
            }
        });
    }

    public void regisration(final Activity activity, final FirebaseAuth mAuth, final Saver saver, final boolean isLogined,
                            final String emailString, final String passwordString, final String passwordString2){
        if(isLogined){
            if(emailString != null && !emailString.isEmpty()
                    && passwordString != null && !passwordString.isEmpty()){
                if(isValidEmail(emailString)) {
                    loginReg(activity, mAuth, saver, isLogined, emailString, passwordString);
                }else{
                    getViewState().showCookie("Incorrect e-mail", "Ups!");
                }

            }else{
                getViewState().showCookie("Empty fields", "Ups!");
            }
        }else {
            if (emailString != null && !emailString.isEmpty()
                    && passwordString != null && !passwordString.isEmpty()) {
                if (isValidEmail(emailString)) {
                    if (passwordString.equals(passwordString2)) {
                        CoinApp.getHttpApi().userRegistration(CoinApp.getId(), emailString, emailString).enqueue(new Callback<Registration>() {
                            @Override
                            public void onResponse(Call<Registration> call, Response<Registration> response) {
                                Registration reg = response.body();
                                int success = -1;
                                if(reg != null){
                                    success = reg.getSuccess();
                                }

                                if(success == 0){
                                    loginReg(activity, mAuth, saver, isLogined, emailString, passwordString);
                                }else{
                                    getViewState().showCloseAppDialog();
                                }
                            }

                            @Override
                            public void onFailure(Call<Registration> call, Throwable t) {
                                DialogsControl.closeProgressDialog();
                                getViewState().showCookie("An error occurred during networking", "Ups!");
                            }
                        });
                    } else {
                        getViewState().showCookie("Incorrect password", "Ups!");
                    }

                } else {
                    getViewState().showCookie("Incorrect e-mail", "Ups!");
                }

            } else {
                getViewState().showCookie("Empty fields", "Ups!");
            }

        }
    }

    public boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    public void forgotPass(String emailString){
        if(emailString != null && !emailString.isEmpty()){
            if(isValidEmail(emailString)) {
                getViewState().showDialogForgotPass();
            }else{
                getViewState().showCookie("Incorrect e-mail", "Ups!");
            }
        }else{
            getViewState().showCookie("Incorrect e-mail", "Ups!");
        }
    }

    public void loginReg(final Activity activity, FirebaseAuth mAuth, final Saver saver, boolean isLogined, final String emailString,
                         final String passwordString){
        getViewState().startProgress();
        if(isLogined){
            mAuth.signInWithEmailAndPassword(emailString, passwordString)
                    .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            DialogsControl.closeProgressDialog();
                            if (!task.isSuccessful()) {
                                getViewState().showCookie(task.getException().getMessage(), "");
                            }else{
                                saver.setMail(emailString);
                                saver.setPassword(passwordString);
                                CoinApp.getHttpApi().userRegistration(CoinApp.getId(), emailString, emailString).enqueue(new Callback<Registration>() {
                                    @Override
                                    public void onResponse(Call<Registration> call, Response<Registration> response) {
                                        Registration reg = response.body();
                                        int success = -1;
                                        if(reg != null){
                                            success = reg.getSuccess();
                                        }

                                        if(success == 0){
                                            getBackup(activity);
                                        }else{
                                            getViewState().showCloseAppDialog();
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Registration> call, Throwable t) {
                                        DialogsControl.closeProgressDialog();
                                        getViewState().showCookie("An error occurred during networking", "Ups!");
                                    }
                                });
                            }
                        }
                    });
        }else{
            mAuth.createUserWithEmailAndPassword(emailString, passwordString)
                    .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            DialogsControl.closeProgressDialog();
                            if (!task.isSuccessful()) {
                                getViewState().showCookie(task.getException().getMessage(), "");
                            } else {
                                saver.setMail(emailString);
                                saver.setPassword(passwordString);
                                getViewState().goToApp();
                            }
                        }
                    });
        }
    }

    public void getBackup(final Activity activity){
        DialogsControl.startProgressDialog(activity);
        CoinApp.getHttpApi().getBackup(CoinApp.getId())
                .enqueue(new Callback<BackupFromServer>() {
                    @Override
                    public void onResponse(Call<BackupFromServer> call, Response<BackupFromServer> response) {
                        BackupFromServer auth = response.body();
                        int success = -1;
                        if(auth != null){
                            success = auth.getSuccess();
                        }

                        if(success == 2){
                            Gson gson = new Gson();
                            Backup[] backups = gson.fromJson(auth.getMessage(), Backup[].class);
                            if(backups != null && backups.length > 0){
                                for(int i = 0; i < backups.length; i++){
                                    RealmController.setMyCoin(backups[i]);
                                }
                            }
                            getViewState().goToApp();
                            DialogsControl.closeProgressDialog();
                        }else{
                            DialogsControl.showCloseAppDialog(activity);
                        }
                    }

                    @Override
                    public void onFailure(Call<BackupFromServer> call, Throwable t) {
                        DialogsControl.showCloseAppDialog(activity);
                        DialogsControl.closeProgressDialog();
                    }
                });
    }
}
