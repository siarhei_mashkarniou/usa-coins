package kamar.zygote.usacoins.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import io.realm.RealmResults;
import kamar.zygote.usacoins.model.CollectionModel;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface CatalogView extends MvpView{

    void setDataToList(RealmResults<CollectionModel> collections);
}
