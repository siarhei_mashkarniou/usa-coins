package kamar.zygote.usacoins.mvp.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import io.realm.RealmResults;
import kamar.zygote.usacoins.database.RealmController;
import kamar.zygote.usacoins.model.Backup;
import kamar.zygote.usacoins.model.CoinModel;
import kamar.zygote.usacoins.mvp.view.CollectionCatalogView;

@InjectViewState
public class CollectionCatalogPresenter extends MvpPresenter<CollectionCatalogView> {

    private RealmResults<CoinModel> coins;
    private RealmResults<Backup> myCoins;

    public void getData(int ID){
        coins = RealmController.getCoinsById(ID);
        myCoins = RealmController.getMyCoinsById(ID);

        for(int i = 0; i < coins.size(); i++){
            for(int j = 0; j < myCoins.size(); j++) {
                if (coins.get(i).getId() == myCoins.get(j).getCoinId()) {
                    RealmController.getRealm().beginTransaction();
                    coins.get(i).setMyCoin(myCoins.get(j));
                    RealmController.getRealm().commitTransaction();
                }
            }
        }

        getViewState().setDataToList(coins);
    }

}
