package kamar.zygote.usacoins.mvp.view;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface StartView extends MvpView {

    void showRegistration();
    void goToApp();
    void showCookie(String message, String top);
    void showDialogForgotPass();
    void startProgress();
    void showCloseAppDialog();
    void showLogin();

}
