package kamar.zygote.usacoins.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.core.CoinApp;
import kamar.zygote.usacoins.model.CoinModel;

public class CollectionCatalogAdapter extends RecyclerView.Adapter<CollectionCatalogAdapter.ViewHolder>{

    private OnItemClickListener listener;
    private RealmResults<CoinModel> coins;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.starImage)
        ImageView starImage;
        @BindView(R.id.noteImage)
        ImageView noteImage;
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.years)
        TextView years;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final CoinModel item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public CollectionCatalogAdapter(RealmResults<CoinModel> coins, CollectionCatalogAdapter.OnItemClickListener listener) {
        this.coins = coins;
        this.listener = listener;
    }

    @Override
    public CollectionCatalogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_catalog_collection, parent, false);
        ViewHolder holder = new ViewHolder(root);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.bind(coins.get(position), listener);

        if(coins.get(position).getMyCoin() != null && coins.get(position).getMyCoin().getNote() != null &&
                coins.get(position).getMyCoin().getNote().trim().length() != 0){
            holder.noteImage.setVisibility(View.VISIBLE);
        }else{
            holder.noteImage.setVisibility(View.GONE);
        }

        if(getMint(position)){
            holder.starImage.setVisibility(View.VISIBLE);
        }else{
            holder.starImage.setVisibility(View.GONE);
        }

        holder.text.setText(coins.get(position).getName());
        holder.years.setText(coins.get(position).getYear());

        Glide.with(CoinApp.getContext())
                .load(coins.get(position).getImage_2())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return coins.size();
    }

    public interface OnItemClickListener {
        void onItemClick(CoinModel item);
    }

    private boolean getMint(int position){
        boolean vis = false;
        int visCounter = 0;
        if(coins.get(position).getMyCoin() != null){
            for(int i = 0; i < coins.get(position).getMyCoin().getCount().size(); i++){
                if(coins.get(position).getMyCoin().getCount().get(i) != null){
                    if(coins.get(position).getMyCoin().getCount().get(i).getCount() > 0){
                        visCounter++;
                    }
                }
            }
            if(visCounter > 0){
                vis = true;
            }
        }

        return vis;
    }
}
