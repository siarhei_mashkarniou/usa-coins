package kamar.zygote.usacoins.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.core.CoinApp;
import kamar.zygote.usacoins.model.CollectionModel;


public class CatalogAdapter extends RecyclerView.Adapter<CatalogAdapter.ViewHolder>{

    private RealmResults<CollectionModel> collections;
    private OnItemClickListener listener;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.years)
        TextView years;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final CollectionModel item, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public CatalogAdapter(RealmResults<CollectionModel> collections, OnItemClickListener listener) {
        this.collections = collections;
        this.listener = listener;
    }

    @Override
    public CatalogAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_catalog, parent, false);
        ViewHolder holder = new ViewHolder(root);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.bind(collections.get(position), listener);

        holder.text.setText(collections.get(position).getName());
        holder.years.setText(collections.get(position).getYears());

        Glide.with(CoinApp.getContext())
                .load(collections.get(position).getImage())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return collections.size();
    }

    public interface OnItemClickListener {
        void onItemClick(CollectionModel item);
    }
}
