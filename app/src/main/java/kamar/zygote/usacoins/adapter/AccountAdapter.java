package kamar.zygote.usacoins.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import kamar.zygote.usacoins.R;

public class AccountAdapter extends BaseAdapter {

    private LayoutInflater lInflater;
    private ArrayList<String> data;

    public AccountAdapter(Context context, ArrayList<String> data) {
        this.data = data;
        this.lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = lInflater.inflate(R.layout.item_account, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        holder.text.setText(data.get(position));

        if(position == 0){
            holder.image.setBackgroundResource(R.drawable.ic_restore_black_24dp);
        }else if(position == 1){
            holder.image.setBackgroundResource(R.drawable.ic_exit_to_app_black_24dp);
        }

        return view;
    }

    static class ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.text)
        TextView text;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
