package kamar.zygote.usacoins.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmResults;
import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.core.CoinApp;
import kamar.zygote.usacoins.model.Backup;
import kamar.zygote.usacoins.model.CoinModel;
import kamar.zygote.usacoins.model.CollectionModel;
import kamar.zygote.usacoins.ui.CoinsTextView;

public class MyCollectionAdapter extends BaseAdapter {

    private LayoutInflater lInflater;
    private RealmResults<CollectionModel> collections;
    private RealmResults<CoinModel> coins;
    private RealmResults<Backup> myCoins;

    public MyCollectionAdapter(Activity context, RealmResults<CollectionModel> collections, RealmResults<CoinModel> coins,
                               RealmResults<Backup> myCoins) {
        this.collections = collections;
        this.coins = coins;
        this.myCoins = myCoins;
        this.lInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return collections.size();
    }

    @Override
    public Object getItem(int position) {
        return collections.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ViewHolder holder;
        if (view != null) {
            holder = (ViewHolder) view.getTag();
        } else {
            view = lInflater.inflate(R.layout.item_my_collection, parent, false);
            holder = new ViewHolder(view);
            view.setTag(holder);
        }

        int max = getMax(position);
        int[] total = getTotal(position);

        holder.progressBar.setMax(max);
        holder.textProgress.setText(total[1] + "/" + max);
        holder.progressBar.setProgress(total[1]);
        holder.textTotalCoins.setText("" + total[0]);

        holder.name.setText(collections.get(position).getName() + "\n" + collections.get(position).getYears());

        Glide.with(CoinApp.getContext()).load(collections.get(position).getImage()).diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.coin_ic).into(holder.image);

        return view;
    }

    private int[] getTotal(int position){
        int[] total = {0, 0};
        for(Backup m : myCoins.where().equalTo("collectionId", collections.get(position).getId()).findAll()){
            for(int i = 0; i < m.getCount().size(); i++){
                total[0] = total[0] + m.getCount().get(i).getCount();
                total[1] = (m.getCount().get(i).getCount() > 0)? total[1] + 1 : total[1];
            }
        }
        return total;
    }

    private int getMax(int position){
        return coins.where().equalTo("id_collection", collections.get(position).getId()).findAll().size();
    }

    class ViewHolder {
        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.text)
        CoinsTextView name;
        @BindView(R.id.progressBar)
        ProgressBar progressBar;
        @BindView(R.id.textProgress)
        TextView textProgress;
        @BindView(R.id.textTotalCoins)
        TextView textTotalCoins;
        @BindView(R.id.textTotalPrice)
        TextView textTotalPrice;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
