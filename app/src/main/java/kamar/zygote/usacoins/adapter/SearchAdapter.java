package kamar.zygote.usacoins.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.RealmList;
import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.core.CoinApp;
import kamar.zygote.usacoins.model.CoinModel;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private RealmList<CoinModel> coins;
    private SearchAdapter.OnItemClickListener listener;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.image)
        ImageView image;
        @BindView(R.id.text)
        TextView name;
        @BindView(R.id.years)
        TextView date;
        @BindView(R.id.textMetall)
        TextView metall;
        @BindView(R.id.startImage)
        ImageView starImage;
        @BindView(R.id.noteImage)
        ImageView noteImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final CoinModel item, final SearchAdapter.OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }
    }

    public SearchAdapter(RealmList<CoinModel> coins, SearchAdapter.OnItemClickListener listener) {
        this.coins = coins;
        this.listener = listener;
    }

    @Override
    public SearchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_search, parent, false);
        ViewHolder holder = new ViewHolder(root);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        holder.bind(coins.get(position), listener);

        if(coins.get(position).getMyCoin() != null && coins.get(position).getMyCoin().getNote() != null &&
                coins.get(position).getMyCoin().getNote().trim().length() != 0){
            holder.noteImage.setVisibility(View.VISIBLE);
        }else{
            holder.noteImage.setVisibility(View.GONE);
        }

        if(getMint(position)){
            holder.starImage.setVisibility(View.VISIBLE);
        }else{
            holder.starImage.setVisibility(View.GONE);
        }

        holder.name.setText(Html.fromHtml(coins.get(position).getName()));
        holder.date.setText(Html.fromHtml(coins.get(position).getYear()));
        holder.metall.setText(Html.fromHtml(coins.get(position).getMetall()));

        Glide.with(CoinApp.getContext())
                .load(coins.get(position).getImage_2())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.image);

    }

    @Override
    public int getItemCount() {
        return coins.size();
    }

    public interface OnItemClickListener {
        void onItemClick(CoinModel item);
    }

    private boolean getMint(int position){
        boolean vis = false;
        int visCounter = 0;
        if(coins.get(position).getMyCoin() != null){
            for(int i = 0; i < coins.get(position).getMyCoin().getCount().size(); i++){
                if(coins.get(position).getMyCoin().getCount().get(i).getCount() > 0){
                    visCounter++;
                }
            }
            if(visCounter > 0){
                vis = true;
            }
        }
        return vis;
    }
}
