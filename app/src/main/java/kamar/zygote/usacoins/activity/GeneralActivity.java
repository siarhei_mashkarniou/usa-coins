package kamar.zygote.usacoins.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;

import org.aviran.cookiebar2.CookieBar;

import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.core.DialogsControl;
import kamar.zygote.usacoins.core.FragmentIvents;
import kamar.zygote.usacoins.core.FragmentsArray;
import kamar.zygote.usacoins.core.ToolbarController;
import kamar.zygote.usacoins.database.RealmController;
import kamar.zygote.usacoins.fragment.AccountFragment;
import kamar.zygote.usacoins.fragment.CatalogFragment;
import kamar.zygote.usacoins.fragment.CoinFragment;
import kamar.zygote.usacoins.fragment.CollectionCatalogFragment;
import kamar.zygote.usacoins.fragment.MyCollectionFragment;
import kamar.zygote.usacoins.fragment.SearchFragment;
import kamar.zygote.usacoins.ui.BottomNavigationViewHelper;

public class GeneralActivity extends AppCompatActivity implements FragmentIvents, BottomNavigationView.OnNavigationItemSelectedListener {

    private Fragment fragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private BottomNavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general);

        ToolbarController.initToolbar(this);

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);
        BottomNavigationViewHelper.removeShiftMode(navigation);

        Menu m = navigation.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu!=null && subMenu.size() >0 ) {
                for (int j=0; j <subMenu.size();j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem);
                }
            }
            applyFontToMenuItem(mi);
        }

        fragmentManager = getSupportFragmentManager();
        navigation.setSelectedItemId(R.id.navigation_catalog);
    }

    public void onBackPressed() {
        if (fragment instanceof CatalogFragment) {
            DialogsControl.showCloseAppDialog(GeneralActivity.this);
        }else if(fragment instanceof CollectionCatalogFragment || fragment instanceof CoinFragment) {
            super.onBackPressed();
        }else{
            displayView(FragmentsArray.CATALOG_FRAGMENT, -1);
            navigation.setSelectedItemId(R.id.navigation_catalog);
        }
    }

    private void displayView(FragmentsArray fa, int id) {
        Bundle args = new Bundle();
        args.putInt("id", id);
        switch (fa){
            case CATALOG_FRAGMENT:
                fragment = new CatalogFragment();
                break;
            case ACCOUNT_FRAGMENT:
                fragment = new AccountFragment();
                break;
            case COLLECTION_CATALOG_FRAGMENT:
                fragment = new CollectionCatalogFragment();
                fragment.setArguments(args);
                break;
            case COIN_FRAGMENT:
                fragment = new CoinFragment();
                fragment.setArguments(args);
                break;
            case SEARCH_FRAGMENT:
                fragment = new SearchFragment();
                break;
            case COLLECTION_FRAGMENT:
                fragment = new MyCollectionFragment();
                break;
            default:
                break;
        }

        if (fragment != null) {
            startFragment(fragment);
        }
    }

    private void startFragment(Fragment fragment) {
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        if(fragment instanceof CollectionCatalogFragment || fragment instanceof CoinFragment){
            fragmentTransaction.addToBackStack("stack");
        }
        fragmentTransaction.commit();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        RealmController.closeRealm();
    }

    @Override
    public void onClickFragment(FragmentsArray fragmentId, int id) {
        displayView(fragmentId, id);
    }

    @Override
    public void setCurrentFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        fragmentManager.popBackStack();
        switch (item.getItemId()) {
            case R.id.navigation_search:
                displayView(FragmentsArray.SEARCH_FRAGMENT, -1);
                return true;
            case R.id.navigation_catalog:
                displayView(FragmentsArray.CATALOG_FRAGMENT, -1);
                return true;
            case R.id.navigation_collection:
                displayView(FragmentsArray.COLLECTION_FRAGMENT, -1);
                return true;
            case R.id.navigation_account:
                displayView(FragmentsArray.ACCOUNT_FRAGMENT, -1);
                return true;
            case R.id.navigation_forum:
                showCookie("In the following updates");
                return true;
        }
        return false;
    }

    public void showCookie(String message){
        DialogsControl.closeProgressDialog();
        CookieBar.Build(GeneralActivity.this)
                .setTitle("Ups!")
                .setMessage(message)
                .setDuration(3000)
                .show();
    }

    private void applyFontToMenuItem(MenuItem mi) {
//        Typeface font = Typeface.createFromAsset(activity.getAssets(), "fonts/font.ttf");
//        SpannableString mNewTitle = new SpannableString(mi.getTitle());
//        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//        mi.setTitle(mNewTitle);
    }
}
