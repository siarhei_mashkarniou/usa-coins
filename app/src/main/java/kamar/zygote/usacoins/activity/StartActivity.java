package kamar.zygote.usacoins.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.aviran.cookiebar2.CookieBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.core.CoinApp;
import kamar.zygote.usacoins.core.CoinOpenerController;
import kamar.zygote.usacoins.core.DialogsControl;
import kamar.zygote.usacoins.core.Saver;
import kamar.zygote.usacoins.mvp.presenter.StartPresenter;
import kamar.zygote.usacoins.mvp.view.StartView;

public class StartActivity extends MvpAppCompatActivity implements StartView{

    @BindView(R.id.input_email)
    EditText email;
    @BindView(R.id.input_password)
    EditText password;
    @BindView(R.id.input_password_2)
    EditText password2;
    @BindView(R.id.link_signup)
    TextView singnup;
    @BindView(R.id.btn_login)
    Button login;
    @BindView(R.id.pass2)
    TextInputLayout pass2;
    @BindView(R.id.link_forgot_password)
    TextView link_forgot_password;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private Saver saver;
    private boolean isLogined = true;
    @InjectPresenter
    StartPresenter startPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);

        saver = new Saver(CoinApp.getContext());
        mAuth = FirebaseAuth.getInstance();

        CoinOpenerController.initController(this);

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {

                } else {

                }
            }
        };

        if(!saver.getMail().equals("") && !saver.getPassword().equals("")){
            goToApp();
        }

        InputMethodManager keyboard = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        keyboard.hideSoftInputFromWindow(email.getWindowToken(), 0);

        startPresenter.setAuth();

    }

    @OnClick(R.id.link_forgot_password)
    void onClickForgot(){
        startPresenter.forgotPass(email.getText().toString().replaceAll("\\s",""));
    }

    @OnClick(R.id.link_signup)
    void onClickLink(){
        if(isLogined){
            showRegistration();
        }else{
            showLogin();
        }
    }

    @OnClick(R.id.btn_login)
    void onClick(){
        startPresenter.regisration(this, mAuth, saver, isLogined, email.getText().toString().replaceAll("\\s",""),
                password.getText().toString(), password2.getText().toString());

    }

    @Override
    public void showCookie(String message, String top){
        DialogsControl.closeProgressDialog();
        CookieBar.Build(StartActivity.this)
                .setTitle(top)
                .setMessage(message)
                .setDuration(3000)
                .show();
    }

    @Override
    public void showDialogForgotPass() {
        DialogsControl.showForgotPasswordDialog(StartActivity.this, email.getText().toString().replaceAll("\\s",""));
    }

    @Override
    public void startProgress() {
        DialogsControl.startProgressDialog(StartActivity.this);
    }

    @Override
    public void showCloseAppDialog() {
        DialogsControl.showErrorCloseAppDialog(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    @Override
    public void onBackPressed() {
        DialogsControl.showCloseAppDialog(StartActivity.this);
    }

    @Override
    public void showRegistration() {
        pass2.setVisibility(View.VISIBLE);
        link_forgot_password.setVisibility(View.GONE);
        singnup.setText("Login");
        login.setText("Registration");
        isLogined = false;
    }

    @Override
    public void showLogin() {
        pass2.setVisibility(View.GONE);
        link_forgot_password.setVisibility(View.VISIBLE);
        singnup.setText("No account yet? Create one");
        login.setText("Login");
        isLogined = true;
    }

    @Override
    public void goToApp() {
        Intent intent = new Intent(StartActivity.this, GeneralActivity.class);
        startActivity(intent);
        finish();
    }
}
