package kamar.zygote.usacoins.core;

import android.content.Context;
import android.content.SharedPreferences;

public class Saver {

    final static String FILE_NAME = "preferencesCoins";
    final static String PREF_ID = "email";
    final static String PREF_PASS = "pass";
    private SharedPreferences preferences;

    public Saver(Context context) {
        preferences = context.getSharedPreferences(FILE_NAME, 0);
    }

    private SharedPreferences.Editor getEditor() {
        return preferences.edit();
    }

    public void setMail(String email) {
        getEditor().putString(PREF_ID, email).commit();
    }

    public String getMail() {
        return preferences.getString(PREF_ID, "");
    }

    public void setPassword(String pass) {
        getEditor().putString(PREF_PASS, pass).commit();
    }

    public String getPassword() {
        return preferences.getString(PREF_PASS, "");
    }

}
