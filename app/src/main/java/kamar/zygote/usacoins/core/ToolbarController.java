package kamar.zygote.usacoins.core;

import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.widget.TextView;

import java.lang.reflect.Field;

import kamar.zygote.usacoins.R;
import kamar.zygote.usacoins.activity.GeneralActivity;

public class ToolbarController {

    private static Toolbar toolbar;

    public static void setToolbarTitle(String string){
        toolbar.setTitle(string);
    }

    public static void initToolbar(GeneralActivity activity){
        toolbar = (Toolbar) activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);

        getActionBarTextView().setTextColor(activity.getResources().getColor(R.color.kprogresshud_default_color));
        getActionBarTextView().setMaxLines(2);
        getActionBarTextView().setSingleLine(false);
        getActionBarTextView().setGravity(Gravity.CENTER);
        getActionBarTextView().setTextSize(14);
        //Typeface font = Typeface.createFromAsset(activity.getAssets(), "fonts/font.ttf");
       // getActionBarTextView().setTypeface(font);
    }

    private static TextView getActionBarTextView() {
        TextView titleTextView = null;

        try {
            Field f = toolbar.getClass().getDeclaredField("mTitleTextView");
            f.setAccessible(true);
            titleTextView = (TextView) f.get(toolbar);
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        }
        return titleTextView;
    }

}
