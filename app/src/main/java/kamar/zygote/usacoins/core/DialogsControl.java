package kamar.zygote.usacoins.core;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.awesomedialog.blennersilva.awesomedialoglibrary.AwesomeSuccessDialog;
import com.awesomedialog.blennersilva.awesomedialoglibrary.interfaces.Closure;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.aviran.cookiebar2.CookieBar;

import kamar.zygote.usacoins.R;

public class DialogsControl {

    private static KProgressHUD dialog;

    public static void startProgressDialog(Context context) {
        dialog = KProgressHUD.create(context)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0f)
                .show();
    }

    public static void closeProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    public static void showCloseAppDialog(final Activity context) {
        new AwesomeSuccessDialog(context)
                .setTitle(R.string.exit)
                .setMessage(R.string.exit_desc)
                .setColoredCircle(R.color.dialogNoticeBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_warning, R.color.white)
                .setCancelable(true)
                .setPositiveButtonText(context.getResources().getString(R.string.ok))
                .setPositiveButtonbackgroundColor(R.color.dialogNoticeBackgroundColor)
                .setPositiveButtonTextColor(R.color.white)
                .setNegativeButtonText(context.getResources().getString(R.string.cancel))
                .setNegativeButtonbackgroundColor(R.color.dialogNoticeBackgroundColor)
                .setNegativeButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        context.finish();
                    }
                })
                .setNegativeButtonClick(new Closure() {
                    @Override
                    public void exec() {

                    }
                })
                .show();
    }

    public static void showPicDialog(final Activity context, final Fragment fragment) {
        final PhotoChoser photoChoser = (PhotoChoser) fragment;
        new AwesomeSuccessDialog(context)
                .setTitle(R.string.photo)
                .setMessage(R.string.photo2)
                .setColoredCircle(R.color.dialogNoticeBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_info, R.color.white)
                .setCancelable(true)
                .setDoneButtonText(context.getResources().getString(R.string.cancel))
                .setDoneButtonbackgroundColor(R.color.dialogNoticeBackgroundColor)
                .setDoneButtonTextColor(R.color.white)
                .setPositiveButtonText(context.getResources().getString(R.string.camera))
                .setPositiveButtonbackgroundColor(R.color.dialogNoticeBackgroundColor)
                .setPositiveButtonTextColor(R.color.white)
                .setNegativeButtonText(context.getResources().getString(R.string.galery))
                .setNegativeButtonbackgroundColor(R.color.dialogNoticeBackgroundColor)
                .setNegativeButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        photoChoser.camera();
                    }
                })
                .setNegativeButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        photoChoser.galery();
                    }
                })
                .setDoneButtonClick(new Closure() {
                    @Override
                    public void exec() {

                    }
                })
                .show();
    }

    public static void showErrorCloseAppDialog(final Activity context) {
        new AwesomeSuccessDialog(context)
                .setTitle("Error")
                .setMessage("Please try again later")
                .setColoredCircle(R.color.dialogNoticeBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_error, R.color.white)
                .setCancelable(false)
                .setPositiveButtonText(context.getResources().getString(R.string.ok))
                .setPositiveButtonbackgroundColor(R.color.dialogNoticeBackgroundColor)
                .setPositiveButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        context.finish();
                    }
                })
                .show();
    }

    public static void showForgotPasswordDialog(final Activity context, final String email) {
        new AwesomeSuccessDialog(context)
                .setTitle("Forgot password?")
                .setMessage("Send reset email to " + email + "?")
                .setColoredCircle(R.color.dialogNoticeBackgroundColor)
                .setDialogIconAndColor(R.drawable.ic_dialog_warning, R.color.white)
                .setCancelable(false)
                .setPositiveButtonText(context.getResources().getString(R.string.ok))
                .setPositiveButtonbackgroundColor(R.color.dialogNoticeBackgroundColor)
                .setPositiveButtonTextColor(R.color.white)
                .setNegativeButtonText(context.getResources().getString(R.string.cancel))
                .setNegativeButtonbackgroundColor(R.color.dialogNoticeBackgroundColor)
                .setNegativeButtonTextColor(R.color.white)
                .setPositiveButtonClick(new Closure() {
                    @Override
                    public void exec() {
                        FirebaseAuth auth = FirebaseAuth.getInstance();
                        auth.sendPasswordResetEmail(email)
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        if (task.isSuccessful()) {
                                            showCookie(context, "ok");
                                        }else{
                                            showCookie(context, task.getException().getMessage());
                                        }
                                    }
                                });
                    }
                })
                .setNegativeButtonClick(new Closure() {
                    @Override
                    public void exec() {

                    }
                })
                .show();
    }

    private static void showCookie(final Activity context, String message){
        CookieBar.Build(context)
                .setTitle("Ups!")
                .setMessage(message)
                .setDuration(3000)
                .show();
    }

    public interface PhotoChoser{
        void galery();
        void camera();
    }
}
