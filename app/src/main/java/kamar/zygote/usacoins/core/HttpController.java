package kamar.zygote.usacoins.core;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HttpController {

    static final String BASE_URL = "http://kam32ar.com";

    public static HttpApi getApi() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        HttpApi umoriliApi = retrofit.create(HttpApi.class);
        return umoriliApi;

    }
}
