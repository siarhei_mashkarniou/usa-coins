package kamar.zygote.usacoins.core;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import kamar.zygote.usacoins.database.RealmMigrations;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class CoinApp extends MultiDexApplication {

    private static Context context;
    private Retrofit retrofit;
    private static HttpApi httpApi;

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        context = this;
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().name("coins.realm")
                .schemaVersion(1).migration(new RealmMigrations()).build();
        Realm.setDefaultConfiguration(config);

        retrofit = new Retrofit.Builder()
                .baseUrl("http://kam32ar.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        httpApi = retrofit.create(HttpApi.class);
    }

    public static HttpApi getHttpApi(){
        return httpApi;
    }

    public static Context getContext(){
        return context;
    }

    public static String getId() {
        String id = "";
        String pack = getContext().getPackageName();
        String and_id = Settings.Secure.getString(getContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        id = md5(and_id + pack);

        return id;
    }

    public static String getVersion() {
        PackageInfo pInfo = null;
        try {
            pInfo = getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        return pInfo.versionName;
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            MessageDigest digest = MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
