package kamar.zygote.usacoins.core;

public enum FragmentsArray {
    CATALOG_FRAGMENT,
    COLLECTION_FRAGMENT,
    ACCOUNT_FRAGMENT,
    CHAT_FRAGMENT,
    SEARCH_FRAGMENT,
    COLLECTION_CATALOG_FRAGMENT,
    COIN_FRAGMENT
}
