package kamar.zygote.usacoins.core;


import android.app.Activity;

import kamar.zygote.usacoins.database.DatabaseAccess;

public class CoinOpenerController {

    private static DatabaseAccess databaseAccess;

    public static void initController(Activity activity){
        databaseAccess = DatabaseAccess.getInstance(activity);
        databaseAccess.open();
        databaseAccess.getCollections();
        databaseAccess.close();
    }

}
