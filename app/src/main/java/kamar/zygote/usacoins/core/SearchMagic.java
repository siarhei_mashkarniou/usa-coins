package kamar.zygote.usacoins.core;

import java.util.ArrayList;
import java.util.List;

public class SearchMagic {

    public static List<String> getSearch(String search, String name, String metall, String date) {
        List<String> result = new ArrayList<>();
        ArrayList<String> start = new ArrayList<>();
        ArrayList<String> startM = new ArrayList<>();
        ArrayList<String> startD = new ArrayList<>();

        int temp = -1;
        int temp1 = -1;

        int tempM = -1;
        int tempM1 = -1;

        int tempD = -1;
        int tempD1 = -1;

        if (search.length() > 0) {
            temp = name.toLowerCase().indexOf(search.toLowerCase());
            temp1 = 0;
            tempM = metall.toLowerCase().indexOf(search.toLowerCase());
            tempM1 = 0;
            tempD = date.toLowerCase().indexOf(search.toLowerCase());
            tempD1 = 0;
        } else {

        }

        while (name.length() > temp) {
            if (temp != -1) {
                temp1 = name.toLowerCase().indexOf(search.toLowerCase(), temp);
                temp = temp1;
                temp++;
                if (temp1 != -1) {
                    name = new StringBuffer(name).insert(temp1 + (search.length()), "|").toString();
                    start.add(String.valueOf(temp1));
                }
            }
            if (temp1 == -1 || temp == -1) {
                break;
            }

        }

        for (int j = 0; j <= start.size() - 1; j++) {
            StringBuffer namebuffer = new StringBuffer(name).insert(Integer.valueOf(start.get(j)) + j, "*");
            name = namebuffer.toString();
        }

        while (metall.length() > tempM) {
            if (tempM != -1) {
                tempM1 = metall.toLowerCase().indexOf(search.toLowerCase(), tempM);
                tempM = tempM1;
                tempM++;
                if (tempM1 != -1) {
                    metall = new StringBuffer(metall).insert(tempM1 + (search.length()), "|").toString();
                    startM.add(String.valueOf(tempM1));
                }
            }
            if (tempM1 == -1 || tempM == -1) {
                break;
            }

        }

        for (int j = 0; j <= startM.size() - 1; j++) {
            StringBuffer metallbuffer = new StringBuffer(metall).insert(Integer.valueOf(startM.get(j)) + j, "*");
            metall = metallbuffer.toString();
        }

        while (date.length() > tempD) {
            if (tempD != -1) {
                tempD1 = date.toLowerCase().indexOf(search.toLowerCase(), tempD);
                tempD = tempD1;
                tempD++;
                if (tempD1 != -1) {
                    date = new StringBuffer(date).insert(tempD1 + (search.length()), "|").toString();
                    startD.add(String.valueOf(tempD1));
                }
            }
            if (tempD1 == -1 || tempD == -1) {
                break;
            }

        }

        for (int j = 0; j <= startD.size() - 1; j++) {
            StringBuffer datebuffer = new StringBuffer(date).insert(Integer.valueOf(startD.get(j)) + j, "*");
            date = datebuffer.toString();
        }

        name = name.replace("|", "</b></font>");
        name = name.replace("*", "<font color=\"#005875\"><b>");

        metall = metall.replace("|", "</b></font>");
        metall = metall.replace("*", "<font color=\"#005875\"><b>");

        date = date.replace("|", "</b></font>");
        date = date.replace("*", "<font color=\"#005875\"><b>");

        result.add(name);
        result.add(metall);
        result.add(date);

        return result;
    }
}
