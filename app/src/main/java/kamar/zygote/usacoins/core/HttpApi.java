package kamar.zygote.usacoins.core;

import kamar.zygote.usacoins.model.Auth;
import kamar.zygote.usacoins.model.BackupFromServer;
import kamar.zygote.usacoins.model.Registration;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface HttpApi {

    @GET("/reg")
    Call<Registration> userRegistration(@Query("user_id") String user_id, @Query("name") String name, @Query("extid") String extid);

    @GET("/auth")
    Call<Auth> auth(@Query("user_id") String user_id, @Query("package") String pack, @Query("version") String version);

    @GET("/backup")
    Call<BackupFromServer> sendBackup(@Query("user_id") String id, @Query("backup") String backup);

    @GET("/backup")
    Call<BackupFromServer> getBackup(@Query("user_id") String user_id);
}
