package kamar.zygote.usacoins.ui;

import android.content.Context;
import android.util.AttributeSet;

public class TitleTextView extends android.support.v7.widget.AppCompatTextView {

    public TitleTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public TitleTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TitleTextView(Context context) {
        super(context);
        init();
    }

    private void init() {
//        if (!isInEditMode()) {
//            Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/font.ttf");
//            setTypeface(tf);
//        }
    }
}
